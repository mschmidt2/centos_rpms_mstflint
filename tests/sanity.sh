#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=0
cx2="ConnectX VPI PCIe 2.0"
cx3="[ConnectX-3]"
device_list=$(lspci | grep 'Mellanox' | grep -v 'Virtual' | grep -v 'DMA controller' | awk '{print $1}')

# Skip loop if device list is empty
[[ -z "${dev// }" ]] || for dev in "$device_list"; do
    # View Device Info

    lspci -vvv -s "$dev"
    dev_name=$(lspci -s "$dev")

    # Test Mstflint Tools

    mstflint -d "$dev" q
    let ret=$ret+$?

    # mstconfig does not support < 4th Gen devices (e.g. ConnectX-2)
    if [[ "$dev_name" != *"$cx2"* ]]; then
        mstconfig -d "${dev}" q
        let ret=$ret+$?
    fi

    mstvpd "$dev" 2>/dev/null
    let ret=$ret+$?

    mstregdump "$dev" >/dev/null
    let ret=$ret+$?

    # mstfwmanager/mstfwreset do not support ConnectX-2 or ConnectX-3
    if [[ "$dev_name" != *"$cx2"* && "$dev_name" != *"$cx3"* ]]; then
        mstfwmanager -d "$dev" --query
        let ret=$ret+$?

        mstfwreset -d "$dev" q
        let ret=$ret+$?
    fi
done

# end test - it is possible no Mellanox devices were available at all to test,
# in which case we automatically mark the test PASS (exit 0)
exit $ret
